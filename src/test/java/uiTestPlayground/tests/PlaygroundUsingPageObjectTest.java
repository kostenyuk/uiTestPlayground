package uiTestPlayground.tests;

import uiTestPlayground.core.BaseTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import uiTestPlayground.pages.DynamicTablePage;
import uiTestPlayground.pages.PlayGroundMainPage;
import uiTestPlayground.pages.ShadowDomPage;

public class PlaygroundUsingPageObjectTest extends BaseTest {

    private final String baseUrl = "http://uitestingplayground.com/";

    //dynamicIdPage tests
    @Test
    public void checkNotRecordingDynamicIdInlineTest() {
        Assertions.assertTrue(
                new PlayGroundMainPage(baseUrl)
                        .openDynamicIdPAge()
                        .checkButtonExist()
        );
    }

    //classAttributePage tests
    @Test
    public void checkClassAttributeXPathIsRightInlineTest() {
        Assertions.assertTrue(
                new PlayGroundMainPage(baseUrl)
                        .openClassAttributePage()
                        .checkPrimaryButtonExist()
        );
    }

    //hiddenLayersPage tests
    @Test
    public void canNotClickGreenButtonTwiceInlineTest() {
        Assertions.assertTrue(
                new PlayGroundMainPage(baseUrl)
                        .openHiddenLayersPage()
                        .doubleClickGreenButton()
        );
    }

    //delayButtonPage tests
    @Test
    public void checkDelayButtonExistInlineTest() {
        Assertions.assertTrue(
                new PlayGroundMainPage(baseUrl)
                        .openLoadDelayPage()
                        .checkDelayButtonExist()
        );
    }

    //AjaxDataPage tests
    @Test
    public void getSuccessAjaxResponseInlineTest() {
        Assertions.assertTrue(
                new PlayGroundMainPage(baseUrl)
                        .openAjaxDataPage()
                        .getSuccessAjaxResponse()
        );
    }

    //ClientSideDelayPage tests
    @Test
    public void checkDataAppearWithDelayOnClientSideInlineTest() {
        Assertions.assertTrue(
                new PlayGroundMainPage(baseUrl)
                        .openClientSideDelayPage()
                        .clickLoadDataButton()
        );
    }

    //IgnoreDOMClickPage tests
    @Test
    public void buttonShouldChangeColorOnClickInlineTest() {
        Assertions.assertTrue(
                new PlayGroundMainPage(baseUrl)
                        .openIgnoreDOMClickPage()
                        .clickOnBlueButton()
        );
    }

    //TextInputPage tests
    @Test
    public void buttonShouldHaveTextFromInput() {
        Assertions.assertEquals("testText", new PlayGroundMainPage(baseUrl)
                .openTextInputPage()
                .fillInputAndGetTextFromButton("testText"));
    }

    //ScrollBarsPage tests
    @Test
    public void buttonShouldBeVisible() {
        Assertions.assertTrue(new PlayGroundMainPage(baseUrl)
                .openScrollBarsPage()
                .scrollToButton());
    }

    //DynamicTablePage tests
    @Test
    public void checkThatXPathFromDynamicTableIsCorrect() {
        DynamicTablePage dynamicTablePage = new PlayGroundMainPage(baseUrl).openDynamicTablePage();
        Assertions.assertEquals(dynamicTablePage.getExpectedCpuLoad(), dynamicTablePage.getActualCpuLoad());
    }

    //VerifyTextPage tests
    @Test
    public void verifyThatFoundElementByTextExist() {
        Assertions.assertTrue(new PlayGroundMainPage(baseUrl)
                        .openVerifyTextPage()
                        .foundElementOnPage()
                        .exists()
        );
    }


    //ProgressBarPage tests
    @Test
    public void verifyThatDifferenceIs0() {
        Assertions.assertEquals("0",
                new PlayGroundMainPage(baseUrl)
                .openProgressBarPage()
                .startAndStopProgressBarAndGetResultDifference());
    }

    //VisibilityPage tests
    @Test
    public void allButtonsShouldBeNotDisplayed() {
        Assertions.assertFalse(
                new PlayGroundMainPage(baseUrl)
                        .openVisibilityPage()
                        .clickHideButton()
                        .checkAllButtonsShouldBeNotDisplayed()
        );
    }

    //SampleAppPage tests
    @Test
    public void successfulLogIn() {
        String name = "test";
        Assertions.assertEquals("Welcome, " + name + "!",
                new PlayGroundMainPage(baseUrl)
                        .openSampleAppPage()
                        .fillForm(name, "pwd")
                        .getStatusText());
    }

    @Test
    public void unSuccessfulLogIn() {
        String name = "test";
        Assertions.assertEquals("Invalid username/password",
                new PlayGroundMainPage(baseUrl)
                        .openSampleAppPage()
                        .fillForm(name, name)
                        .getStatusText());
    }

    //MouseOverPage tests
    @Test
    public void checkThatLinkIsClickableWithMouseHover() {
        Assertions.assertEquals("2",
                new PlayGroundMainPage(baseUrl)
                        .openMouseOverPage()
                        .clickTwoTimesOnLink()
                        .getClickCount());
    }

    //NbspPage tests
    @Test
    public void checkNbspElementExist() {
        Assertions.assertTrue(new PlayGroundMainPage(baseUrl)
                .openNbspPage()
                .getNbspElement());
    }

    //OverlappedPage tests
    @Test
    public void checkThatTextEnteredInOverlappedInput() {
        Assertions.assertEquals("settings", new PlayGroundMainPage(baseUrl)
                .openOverlappedPage()
                .scrollToNameField()
                .setNameInput("settings")
                .getNameInputValue());
    }

    //ShadowDomPage tests
    @Test
    public void generateAndCopyTextInShadowDom() {
        ShadowDomPage shadowDomPage = new PlayGroundMainPage(baseUrl)
                .openShadowDomPage()
                .generateAndCopyText();
        Assertions.assertEquals(shadowDomPage.getTextFromClipboard(), shadowDomPage.getInputGeneratorFieldValue());
    }
}
