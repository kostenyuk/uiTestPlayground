package uiTestPlayground.tests;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ex.UIAssertionError;
import uiTestPlayground.core.BaseTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.time.Duration;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.shadowCss;
import static com.codeborne.selenide.Selenide.*;

public class PlaygroundTest extends BaseTest {
    //Pages
    private final SelenideElement dynamicIdPage = $x("//a[@href=\"/dynamicid\"]");
    private final SelenideElement classAttributePage = $x("//a[@href=\"/classattr\"]");
    private final SelenideElement hiddenLayersPage = $x("//a[@href=\"/hiddenlayers\"]");
    private final SelenideElement loadDelayPage = $x("//a[@href=\"/loaddelay\"]");
    private final SelenideElement ajaxDataPage = $x("//a[@href=\"/ajax\"]");
    private final SelenideElement clientDelayPage = $x("//a[@href=\"/clientdelay\"]");
    private final SelenideElement ignoreDOMClickPage = $x("//a[@href=\"/click\"]");
    private final SelenideElement textInputPage = $x("//a[@href=\"/textinput\"]");
    private final SelenideElement scrollBarsPage = $x("//a[@href=\"/scrollbars\"]");
    private final SelenideElement dynamicTablePage = $x("//a[@href=\"/dynamictable\"]");
    private final SelenideElement verifyTextPage = $x("//a[@href=\"/verifytext\"]");
    private final SelenideElement progressBarPage = $x("//a[@href=\"/progressbar\"]");
    private final SelenideElement visibilityPage = $x("//a[@href=\"/visibility\"]");
    private final SelenideElement sampleAppPage = $x("//a[@href=\"/sampleapp\"]");
    private final SelenideElement mouseOverPage = $x("//a[@href=\"/mouseover\"]");
    private final SelenideElement nbspPage = $x("//a[@href=\"/nbsp\"]");
    private final SelenideElement overlappedPage = $x("//a[@href=\"/overlapped\"]");
    private final SelenideElement shadowDomPage = $x("//a[@href=\"/shadowdom\"]");

    //Elements on pages
    private final SelenideElement requestButton = $x("//button[@class='btn btn-primary']");
    private final SelenideElement buttonWithDynamicId = $x("//button[@class=\"btn btn-primary\"]");
    private final SelenideElement primaryButton = $x("//button[contains(@class, 'btn-primary')]");
    private final SelenideElement greenButton = $x("//button[@class='btn btn-success']");
    private final SelenideElement delayButton = $x("//button[@class='btn btn-primary']");
    private final SelenideElement successAjaxText = $x("//p[text()='Data loaded with AJAX get request.']");
    private final SelenideElement successClientDelayText = $x("//p[text()='Data calculated on the client side.']");
    private final SelenideElement successButton = $x("//button[@class='btn btn-success']");
    private final SelenideElement inputField = $x("//input[@class='form-control']");
    private final SelenideElement expectedCpuLoad = $x("//p[@class='bg-warning']");
    private final SelenideElement actualCpuLoad = $x("//span[text()='Chrome']/following::span[contains(text(), '%')]");
    private final SelenideElement elementByText = $x("//span[contains(normalize-space(), 'Welcome UserName!')]");
    private final SelenideElement startProgressBarButton = $x("//button[@id='startButton']");
    private final SelenideElement stopProgressBarButton = $x("//button[@id='stopButton']");
    private final SelenideElement progressBar= $x("//div[@id='progressBar']");
    private final SelenideElement resultDifference= $x("//p[@id='result']");
    private final SelenideElement removedButton= $x("//button[@id='removedButton']");
    private final SelenideElement zeroWidthButton= $x("//button[@id='zeroWidthButton']");
    private final SelenideElement overlappedButton= $x("//button[@id='overlappedButton']");
    private final SelenideElement transparentButton= $x("//button[@id='transparentButton']");
    private final SelenideElement notDisplayedButton= $x("//button[@id='notdisplayedButton']");
    private final SelenideElement offscreenButton= $x("//button[@id='offscreenButton']");
    private final SelenideElement passwordField= $x("//input[@name='Password']");
    private final SelenideElement usernameField= $x("//input[@name='UserName']");
    private final SelenideElement loginStatus= $x("//label[@id='loginstatus']");
    private final SelenideElement clickMeLink = $x("//a[@class='text-primary']");
    private final SelenideElement clickMeHoverLink = $x("//a[@class='text-warning']");
    private final SelenideElement clickCount = $x("//span[@id='clickCount']");
    private final SelenideElement nbspButton = $(byText("My Button"));
    private final SelenideElement nameInput = $x("//input[@id='name']");
    private final SelenideElement subjectInput = $x("//input[@id='subject']");
    private final SelenideElement generateButton = $(shadowCss("#buttonGenerate", "guid-generator"));
    private final SelenideElement clipboardButton = $(shadowCss("#buttonCopy", "guid-generator"));
    private final SelenideElement inputGeneratorField = $(shadowCss("#editField", "guid-generator"));

    public void openMainPage() {
        String baseUrl = "http://uitestingplayground.com/";
        open(baseUrl);
    }
    //DynamicIdPage tests
    @Test
    public void checkNotRecordingDynamicIdTest() {
        openMainPage();
        dynamicIdPage.click();
        buttonWithDynamicId.click();
        Assertions.assertTrue(buttonWithDynamicId.exists());
    }

    //ClassAttributePage tests
    @Test
    public void checkClassAttributeXPathIsRightTest() {
        openMainPage();
        classAttributePage.click();
        primaryButton.click();
        Selenide.confirm();
        Assertions.assertTrue(primaryButton.exists());
    }

    //HiddenLayersPage tests
    @Test
    public void canNotClickGreenButtonTwiceTest() {
        openMainPage();
        hiddenLayersPage.click();
        greenButton.click();
        try {
            greenButton.click();
        } catch (UIAssertionError e) {
            Assertions.assertTrue(true);
        }
    }

    //LoadDelayPage tests
    @Test
    public void waitingForPageLoadingTest() {
        openMainPage();
        loadDelayPage.click();
        delayButton.click();
        Assertions.assertTrue(delayButton.exists());
    }

    //AjaxDataPage tests
    @Test
    public void getAjaxDataTest() {
        openMainPage();
        ajaxDataPage.click();
        requestButton.click();
        successAjaxText.shouldBe(Condition.exist, Duration.ofSeconds(16));
    }

    //ClientSideDelayPage tests
    @Test
    public void dataAppearAfterDelayOnClientSideTest() {
        openMainPage();
        clientDelayPage.click();
        requestButton.click();
        successClientDelayText.shouldBe(Condition.exist, Duration.ofSeconds(16));
    }

    //IgnoreDOMClickPage tests
    @Test
    public void buttonShouldChangeColorOnClickTest() {
        openMainPage();
        ignoreDOMClickPage.click();
        requestButton.click();
        Assertions.assertTrue(successButton.exists());
    }

    //TextInputPage tests
    @Test
    public void textFromInputShouldBeOnButtonAfterClick() {
        openMainPage();
        textInputPage.click();
        inputField.sendKeys("testText");
        requestButton.click();
        Assertions.assertEquals("testText", requestButton.getText());
    }

    //ScrollBarsPage tests
    @Test
    public void buttonShouldBeVisibleAfterScroll() {
        openMainPage();
        scrollBarsPage.click();
        requestButton.scrollTo();
        requestButton.shouldBe(Condition.visible);
    }

    //DynamicTablePage tests
    @Test
    public void checkingThatXPathFromDynamicTableIsCorrect() {
        openMainPage();
        dynamicTablePage.click();
        Assertions.assertEquals(expectedCpuLoad.getText().replaceAll("[^\\d.]", ""),
                actualCpuLoad.getText().replaceAll("[^\\d.]", ""));
    }

    //VerifyTextPage tests
    @Test
    public void verifyThatFoundElementByTextExist() {
        openMainPage();
        verifyTextPage.click();
        Assertions.assertTrue(elementByText.exists());
    }

    //ProgressBarPage tests
    @Test
    public void verifyThatProgressBarStoppedAt75Percent() {
        openMainPage();
        progressBarPage.click();
        startProgressBarButton.click();
        while (true) {
            if (progressBar.getAttribute("aria-valuenow").equals("75")) {
                stopProgressBarButton.click();
                break;
            }
        }
        //Find a string up to the first comma using a regular expression
        //Remove spaces at the beginning and end of the string (if exist)
        //Remove "Result: " to get a number
        String result = resultDifference.getText()
                .replaceAll("([^,]+),.*", "$1")
                .trim()
                .replaceAll("Result: ", "");
        Assertions.assertEquals("0", result);
    }

    //VisiblePage tests
    @Test
    public void checkThatAllButtonsAreNotVisible() {
        openMainPage();
        visibilityPage.click();
        requestButton.click();
        Assertions.assertFalse(removedButton.isDisplayed() &&
                zeroWidthButton.isDisplayed() &&
                overlappedButton.isDisplayed() &&
                transparentButton.isDisplayed() &&
                notDisplayedButton.isDisplayed() &&
                offscreenButton.isDisplayed());
    }

    //SampleAppPage tests
    @Test
    public void successfulLogIn() {
        String name = "test";
        openMainPage();
        sampleAppPage.click();
        usernameField.setValue(name);
        passwordField.setValue("pwd");
        requestButton.click();
        Assertions.assertEquals("Welcome, " + name + "!", loginStatus.getText());
    }

    @Test
    public void unSuccessfulLogIn() {
        String name = "test";
        openMainPage();
        sampleAppPage.click();
        usernameField.setValue(name);
        passwordField.setValue(name);
        requestButton.click();
        Assertions.assertEquals("Invalid username/password", loginStatus.getText());
    }

    //MouseOverPage tests
    @Test
    public void checkThatLinkIsClickable() {
        openMainPage();
        mouseOverPage.click();
        try {
            clickMeLink.click();
            clickMeLink.click();
        } catch (UIAssertionError e) {
            clickMeHoverLink.click();
        }
        Assertions.assertEquals("2", clickCount.getText());
    }

    //nbspPage tests
    @Test
    public void buttonByText() {
        openMainPage();
        nbspPage.click();
        Assertions.assertTrue(nbspButton.exists());
    }

    //OverlappedPage tests
    @Test
    public void scrollToInputAndRecordText() {
        openMainPage();
        overlappedPage.click();
        subjectInput.scrollIntoView(false);
        nameInput.sendKeys("settings");
        Assertions.assertEquals("settings", nameInput.getValue());
    }

    //ShadowDOMPage tests
    @Test
    public void clipboardValueShouldBeEqualsInputValue() {
        openMainPage();
        shadowDomPage.click();
        generateButton.click();
        // clipboardButton doesn't work on the site
        // clipboardButton.click();
        clipboard().setText(inputGeneratorField.getValue() != null ? inputGeneratorField.getValue() : "");
        Assertions.assertEquals(inputGeneratorField.getValue(), clipboard().getText());
    }
}
