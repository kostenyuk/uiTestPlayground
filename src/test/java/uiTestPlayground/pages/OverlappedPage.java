package uiTestPlayground.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class OverlappedPage {
    private final SelenideElement nameInput = $x("//input[@id='name']");
    private final SelenideElement subjectInput = $x("//input[@id='subject']");

    public OverlappedPage scrollToNameField() {
        subjectInput.scrollIntoView(false);
        return this;
    }

    public OverlappedPage setNameInput(String text) {
        nameInput.sendKeys(text);
        return this;
    }

    public String getNameInputValue() {
        return nameInput.getValue();
    }
}
