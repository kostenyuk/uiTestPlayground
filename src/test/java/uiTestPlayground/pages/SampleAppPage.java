package uiTestPlayground.pages;

import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.Assertions;

import static com.codeborne.selenide.Selenide.$x;

public class SampleAppPage {
    private final SelenideElement requestButton = $x("//button[@class='btn btn-primary']");
    private final SelenideElement passwordField= $x("//input[@name='Password']");
    private final SelenideElement usernameField= $x("//input[@name='UserName']");
    private final SelenideElement loginStatus= $x("//label[@id='loginstatus']");

    public SampleAppPage fillForm(String name, String password) {
        usernameField.setValue(name);
        passwordField.setValue(password);
        requestButton.click();
        return this;
    }

    public String getStatusText() {
        return loginStatus.getText();
    }
}
