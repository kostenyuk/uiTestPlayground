package uiTestPlayground.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$x;

public class ScrollBarsPage {

    private final SelenideElement requestButton = $x("//button[@class='btn btn-primary']");

    public boolean scrollToButton() {
        requestButton.scrollTo();
        try {
            requestButton.shouldBe(Condition.visible);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
