package uiTestPlayground.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$x;

public class AjaxDataPage {
    private final SelenideElement ajaxRequestButton = $x("//button[@class='btn btn-primary']");
    private final SelenideElement successAjaxText = $x("//p[text()='Data loaded with AJAX get request.']");

    public boolean getSuccessAjaxResponse() {
        ajaxRequestButton.click();
        try {
            successAjaxText.shouldBe(Condition.exist, Duration.ofSeconds(16));
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
