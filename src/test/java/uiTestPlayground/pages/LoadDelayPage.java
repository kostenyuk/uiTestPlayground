package uiTestPlayground.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class LoadDelayPage {
    private final SelenideElement delayButton = $x("//button[@class='btn btn-primary']");

    public boolean checkDelayButtonExist() {
        return delayButton.exists();
    }
}
