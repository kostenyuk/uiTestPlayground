package uiTestPlayground.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.shadowCss;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.clipboard;

public class ShadowDomPage {
    private final SelenideElement generateButton = $(shadowCss("#buttonGenerate", "guid-generator"));
    private final SelenideElement clipboardButton = $(shadowCss("#buttonCopy", "guid-generator"));
    private final SelenideElement inputGeneratorField = $(shadowCss("#editField", "guid-generator"));

    public ShadowDomPage generateAndCopyText() {
        generateButton.click();
        clipboard().setText(inputGeneratorField.getValue() != null ? inputGeneratorField.getValue() : "");
        return this;
    }

    public String getInputGeneratorFieldValue() {
        return inputGeneratorField.getValue();
    }

    public String getTextFromClipboard() {
        return clipboard().getText();
    }
}
