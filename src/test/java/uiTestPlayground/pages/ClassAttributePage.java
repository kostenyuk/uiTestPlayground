package uiTestPlayground.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class ClassAttributePage {
    private final SelenideElement primaryButton = $x("//button[contains(@class, 'btn-primary')]");

    public boolean checkPrimaryButtonExist() {
        primaryButton.click();
        Selenide.confirm();
        return primaryButton.exists();
    }
}
