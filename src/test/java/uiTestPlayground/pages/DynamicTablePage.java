package uiTestPlayground.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class DynamicTablePage {
    private final SelenideElement expectedCpuLoad = $x("//p[@class='bg-warning']");
    private final SelenideElement actualCpuLoad = $x("//span[text()='Chrome']/following::span[contains(text(), '%')]");
    private final String notNumbers = "[^\\d.]";

    public String getActualCpuLoad() {
        return actualCpuLoad.getText().replaceAll(notNumbers, "");

    }

    public String getExpectedCpuLoad() {
        return expectedCpuLoad.getText().replaceAll(notNumbers, "");
    }
}
