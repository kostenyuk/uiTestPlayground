package uiTestPlayground.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class VisibilityPage {
    private final SelenideElement removedButton= $x("//button[@id='removedButton']");
    private final SelenideElement zeroWidthButton= $x("//button[@id='zeroWidthButton']");
    private final SelenideElement overlappedButton= $x("//button[@id='overlappedButton']");
    private final SelenideElement transparentButton= $x("//button[@id='transparentButton']");
    private final SelenideElement notDisplayedButton= $x("//button[@id='notdisplayedButton']");
    private final SelenideElement offscreenButton= $x("//button[@id='offscreenButton']");
    private final SelenideElement requestButton = $x("//button[@class='btn btn-primary']");

    public VisibilityPage clickHideButton() {
        requestButton.click();
        return this;
    }

    public boolean checkAllButtonsShouldBeNotDisplayed() {
        return removedButton.isDisplayed() &&
                zeroWidthButton.isDisplayed() &&
                overlappedButton.isDisplayed() &&
                transparentButton.isDisplayed() &&
                notDisplayedButton.isDisplayed() &&
                offscreenButton.isDisplayed();
    }
}
