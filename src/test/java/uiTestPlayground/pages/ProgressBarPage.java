package uiTestPlayground.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class ProgressBarPage {
    private final SelenideElement startProgressBarButton = $x("//button[@id='startButton']");
    private final SelenideElement stopProgressBarButton = $x("//button[@id='stopButton']");
    private final SelenideElement progressBar= $x("//div[@id='progressBar']");
    private final SelenideElement resultDifference= $x("//p[@id='result']");

    public String startAndStopProgressBarAndGetResultDifference() {
        startProgressBarButton.click();
        while (true) {
            if (progressBar.getAttribute("aria-valuenow").equals("75")) {
                stopProgressBarButton.click();
                break;
            }
        }
        return resultDifference.getText()
                .replaceAll("([^,]+),.*", "$1")
                .trim()
                .replaceAll("Result: ", "");
    }
}
