package uiTestPlayground.pages;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ex.UIAssertionError;
import org.junit.jupiter.api.Assertions;

import static com.codeborne.selenide.Selenide.$x;

public class HiddenLayersPage {

    private final SelenideElement greenButton = $x("//button[@class='btn btn-success']");

    public boolean doubleClickGreenButton() {
        greenButton.click();
        try {
            greenButton.click();
        } catch (UIAssertionError e) {
            System.out.println(e.getMessage());
            return true;
        }
        return false;
    }
}
