package uiTestPlayground.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$x;

public class ClientSideDelayPage {
    private final SelenideElement RequestButton = $x("//button[@class='btn btn-primary']");
    private final SelenideElement successClientDelayText = $x("//p[text()='Data calculated on the client side.']");

    public boolean clickLoadDataButton() {
        RequestButton.click();
        try {
            successClientDelayText.shouldBe(Condition.exist, Duration.ofSeconds(16));
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
