package uiTestPlayground.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class VerifyTextPage {
    private final SelenideElement elementByText = $x("//span[contains(normalize-space(), 'Welcome UserName!')]");

    public SelenideElement foundElementOnPage() {
        return elementByText;
    }
}
