package uiTestPlayground.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class PlayGroundMainPage {
    private final SelenideElement dynamicIdPage = $x("//a[@href=\"/dynamicid\"]");
    private final SelenideElement classAttributePage = $x("//a[@href=\"/classattr\"]");
    private final SelenideElement hiddenLayersPage = $x("//a[@href=\"/hiddenlayers\"]");
    private final SelenideElement loadDelayPage = $x("//a[@href=\"/loaddelay\"]");
    private final SelenideElement ajaxDataPage = $x("//a[@href=\"/ajax\"]");
    private final SelenideElement clientDelayPage = $x("//a[@href=\"/clientdelay\"]");
    private final SelenideElement ignoreDOMClickPage = $x("//a[@href=\"/click\"]");
    private final SelenideElement textInputPage = $x("//a[@href=\"/textinput\"]");
    private final SelenideElement scrollBarsPage = $x("//a[@href=\"/scrollbars\"]");
    private final SelenideElement dynamicTablePage = $x("//a[@href=\"/dynamictable\"]");
    private final SelenideElement verifyTextPage = $x("//a[@href=\"/verifytext\"]");
    private final SelenideElement progressBarPage = $x("//a[@href=\"/progressbar\"]");
    private final SelenideElement visibilityPage = $x("//a[@href=\"/visibility\"]");
    private final SelenideElement sampleAppPage = $x("//a[@href=\"/sampleapp\"]");
    private final SelenideElement mouseOverPage = $x("//a[@href=\"/mouseover\"]");
    private final SelenideElement nbspPage = $x("//a[@href=\"/nbsp\"]");
    private final SelenideElement overlappedPage = $x("//a[@href=\"/overlapped\"]");
    private final SelenideElement shadowDomPage = $x("//a[@href=\"/shadowdom\"]");

    public PlayGroundMainPage(String url) {
        Selenide.open(url);
    }

    public DynamicIdPage openDynamicIdPAge() {
        dynamicIdPage.click();
        return new DynamicIdPage();
    }

    public ClassAttributePage openClassAttributePage() {
        classAttributePage.click();
        return new ClassAttributePage();
    }

    public HiddenLayersPage openHiddenLayersPage() {
        hiddenLayersPage.click();
        return new HiddenLayersPage();
    }

    public LoadDelayPage openLoadDelayPage() {
        loadDelayPage.click();
        return new LoadDelayPage();
    }

    public AjaxDataPage openAjaxDataPage() {
        ajaxDataPage.click();
        return new AjaxDataPage();
    }

    public ClientSideDelayPage openClientSideDelayPage() {
        clientDelayPage.click();
        return new ClientSideDelayPage();
    }

    public IgnoreDOMClickPage openIgnoreDOMClickPage() {
        ignoreDOMClickPage.click();
        return new IgnoreDOMClickPage();
    }

    public TextInputPage openTextInputPage() {
        textInputPage.click();
        return new TextInputPage();
    }

    public ScrollBarsPage openScrollBarsPage() {
        scrollBarsPage.click();
        return new ScrollBarsPage();
    }

    public DynamicTablePage openDynamicTablePage() {
        dynamicTablePage.click();
        return new DynamicTablePage();
    }

    public VerifyTextPage openVerifyTextPage() {
        verifyTextPage.click();
        return new VerifyTextPage();
    }

    public ProgressBarPage openProgressBarPage() {
        progressBarPage.click();
        return new ProgressBarPage();
    }

    public VisibilityPage openVisibilityPage() {
        visibilityPage.click();
        return new VisibilityPage();
    }

    public SampleAppPage openSampleAppPage() {
        sampleAppPage.click();
        return new SampleAppPage();
    }

    public MouseOverPage openMouseOverPage() {
        mouseOverPage.click();
        return new MouseOverPage();
    }

    public NbspPage openNbspPage() {
        nbspPage.click();
        return new NbspPage();
    }

    public OverlappedPage openOverlappedPage() {
        overlappedPage.click();
        return new OverlappedPage();
    }

    public ShadowDomPage openShadowDomPage() {
        shadowDomPage.click();
        return new ShadowDomPage();
    }
}
