package uiTestPlayground.pages;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ex.UIAssertionError;

import static com.codeborne.selenide.Selenide.$x;

public class MouseOverPage {
    private final SelenideElement clickMeLink = $x("//a[@class='text-primary']");
    private final SelenideElement clickMeHoverLink = $x("//a[@class='text-warning']");
    private final SelenideElement clickCount = $x("//span[@id='clickCount']");

    public MouseOverPage clickTwoTimesOnLink() {
        try {
            clickMeLink.click();
            clickMeLink.click();
        } catch (UIAssertionError e) {
            clickMeHoverLink.click();
        }
        return this;
    }

    public String getClickCount() {
        return clickCount.getText();
    }
}
