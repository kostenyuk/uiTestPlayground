package uiTestPlayground.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class DynamicIdPage {
    private final SelenideElement buttonWithDynamicId = $x("//button[@class=\"btn btn-primary\"]");

    public boolean checkButtonExist() {
        buttonWithDynamicId.click();
        return buttonWithDynamicId.exists();
    }
}
