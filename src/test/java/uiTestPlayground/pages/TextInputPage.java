package uiTestPlayground.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class TextInputPage {
    private final SelenideElement requestButton = $x("//button[@class='btn btn-primary']");
    private final SelenideElement inputField = $x("//input[@class='form-control']");

    public String fillInputAndGetTextFromButton(String text) {
        inputField.sendKeys(text);
        requestButton.click();
        return requestButton.getText();
    }
}
