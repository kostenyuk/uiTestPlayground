package uiTestPlayground.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class NbspPage {
    private final SelenideElement nbspButton = $(byText("My Button"));

    public boolean getNbspElement() {
        return nbspButton.exists();
    }
}
