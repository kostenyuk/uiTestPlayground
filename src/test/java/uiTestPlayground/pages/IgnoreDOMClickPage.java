package uiTestPlayground.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class IgnoreDOMClickPage {
    private final SelenideElement RequestButton = $x("//button[@class='btn btn-primary']");
    private final SelenideElement successButton = $x("//button[@class='btn btn-success']");

    public boolean clickOnBlueButton() {
        RequestButton.click();
        return successButton.exists();
    }
}
